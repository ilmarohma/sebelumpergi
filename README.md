# SebelumPergi
Projek pembuatan aplikasi ini untuk memenuhi tugas akhir dari mata kuliah Sistem Cerdas (CSCM603130), Fakultas Ilmu Komputer, Universitas Indonesia

## Overview
SebelumPergi adalah aplikasi berbentuk website yang akan memberikan saran apakah seseorang boleh pergi ke suatu wilayah di tengah pandemi Covid-19, atau tidak, melalui self-assessment Covid-19. Aplikasi ini memiliki tujuan agar mencegah besarnya peluang user untuk terinfeksi Covid-19 apabila pergi ke wilayah tujuannya, maupun mencegah peluang user menyebarkan virus Covid-19 ke wilayah tujuannya. 

## Basic Concept
Aplikasi SebelumPergi menggunakan pendekatan Logic-based knowledge representation and reasoning. Ketika user memasukkan input jawaban self-assessment dan pilihan wilayah tujuan, program akan bertanya kepada Knowledge-based Agent mengenai output apa yang akan dikeluarkan jika diketahui input-nya adalah sesuai yang diberikan user tersebut. Informasi yang dimasukkan user diproses oleh inference engine yang dapat menyimpulkan dengan logis sesuai dengan premis yang sudah ditentukan sebelumnya.

## Links
- Data Source : [kawalcovidd19](https://datawrapper.dwcdn.net/BA77E/)

## Developer 
1. [Dinda Inas Putri](https://gitlab.com/dindainas)
2. [Ilma Ainur Rohma](https://gitlab.com/ilmarohma)
3. [Siti Khadijah](https://gitlab.com/sitisk1)
4. [Yasmin Adelia P. C.](https://gitlab.com/yasminadelia)

## Development Guide
1. clone this project
2. make you env 
    ```python -m venv env```
3. install requirements
    ```pip install -r requirements.txt```
4. run project with
    ```python manage.py runserver```
5. create your branch from master
    ```git checkout -b <YOUR-BRANCH-NAME>```
6. Happy Coding :)

## Task List
Check our [issues board](https://gitlab.com/ilmarohma/sebelumpergi/-/boards)