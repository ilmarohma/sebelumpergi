from .data import *

# 0 untuk id jawaban 'tidak'
# 1 untuk id jawaban 'ya' pada pertanyaan QLow
# 2 untuk id jawaban 'ya' pada pertanyaan QMedium
# 3 untuk id jawaban 'ya' pada pertanyaan QHigh

# untuk real nya akan memasukkan id dari tiap jawaban user dari form ke list, 
# lalu hasilnya akan ditentukan dari algoritma yang dilakukan kepada list tersebut

# untuk dummy, dibuat list yang sudah berisi id dari tiap jawaban user
list_jawaban = [0,0,1,1,3,0,0,1,0,1]

def case_assessment(list_kategori_jawaban):
    personCategory = ''
    if 3 in list_kategori_jawaban:
        personCategory = 'high'
    elif 2 in list_kategori_jawaban:
        personCategory = 'medium'
    else:
        personCategory = 'low'
    return personCategory


def check_conclusion(person_status, region):
    allowed = False
    if region in getRHigh():
        print('RHigh')
        allowed = False
    if person_status == 'high':
        allowed = False
    if person_status == 'medium':
        if region in getRMedium():
            print('RMedium')
            allowed = False
        else:
            allowed = True
    if person_status == 'low':
        allowed = True
    return allowed


def inference (list_answer, region):
    status = case_assessment(list_answer)
    return check_conclusion(status, region)

