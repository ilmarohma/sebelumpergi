from django.shortcuts import render, redirect
from .models import *
from .forms import *
from .inference import * 
# Create your views here.

def self_assessment(request):
    	
	if request.method == 'POST':
		demografi_form = FormDemografi(request.POST)
		question_high_form = FormQuestionHigh(request.POST)
		question_medium_form = FormQuestionMedium(request.POST)
		question_low_form = FormQuestionLow(request.POST)
		if demografi_form.is_valid() and question_high_form.is_valid() and question_medium_form.is_valid() and question_low_form.is_valid():
			demografi_form.save()
			question_high_form.save()
			question_medium_form.save()
			question_low_form.save()

			demografi = ModelDemografi.objects.all().last()
			nama = demografi.nama
			tujuan = demografi.daerah_tujuan
			ans_lst = []
			high = ModelQuestionHigh.objects.all().last()
			ans_lst.append(int(high.apakah_dalam_kurun_waktu_14_hari_anda_pernah_bepergian_ke_luar_negeri_atau_daerah_yang_berisiko_covid_tinggi))
			ans_lst.append(int(high.apakah_dalam_kurun_waktu_14_hari_anda_pernah_berkontak_atau_serumah_dengan_orang_yang_positif_covid))
			ans_lst.append(int(high.apakah_dalam_kurun_waktu_14_hari_anda_mengalami_kesulitan_bernafas_nyeri_dada_merasa_sulit_untuk_bangun_dan_penurunan_kesadaran))
			ans_lst.append(int(high.apakah_dalam_kurun_waktu_14_hari_anda_menjadi_pekerja_kesehatan))
			ans_lst.append(int(high.apakah_ada_memiliki_penyakit_kronis_seperti_kardiovaskular_diabetes_penyakit_ginjal_dan_cancer))

			med = ModelQuestionMedium.objects.all().last()
			ans_lst.append(int(med.apakah_dalam_kurun_waktu_14_hari_anda_pernah_berkontak_dengan_orang_yang_PDP))
			ans_lst.append(int(med.apakah_dalam_kurun_waktu_14_hari_anda_pernah_berada_di_kerumunan))
			ans_lst.append(int(med.apakah_dalam_kurun_waktu_14_hari_anda_pernah_keluar_rumah_tanpa_masker))

			low = ModelQuestionLow.objects.all().last()
			ans_lst.append(int(low.apakah_dalam_kurun_waktu_14_hari_anda_pernah_keluar_rumah_dan_menaati_protokol_kesehatan))
			ans_lst.append(int(low.apakah_dalam_kurun_waktu_14_hari_anda_tidak_pernah_keluar_rumah))
			ans_lst.append(int(low.apakah_dalam_kurun_waktu_14_hari_anda_rutin_minum_vitamin_dan_makan_makanan_bergizi))

			context = {
				"hasil": str(inference(ans_lst, tujuan)),
				"tujuan": tujuan
			}

			print(inference(ans_lst, tujuan))
			print(ans_lst)
			return render(request,'result_page.html', context)
	else:
		demografi_form = FormDemografi()
		question_high_form = FormQuestionHigh()
		question_medium_form = FormQuestionMedium()
		question_low_form = FormQuestionLow()

	isi = {'generate_form_demografi':demografi_form,
	'generate_form_high':question_high_form,
	'generate_form_medium':question_medium_form,
	'generate_form_low':question_low_form
	}
	return render(request, 'self-assessment.html', isi)
