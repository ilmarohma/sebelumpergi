from django.urls import path
from . import views

app_name = 'app'

urlpatterns = [
		path('self-assessment/', views.self_assessment, name='self-assessment'),
		
]