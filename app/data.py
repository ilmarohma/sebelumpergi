import csv
import os

data={}
rHigh=[]
rMedium=[]
rLow = []
uncategorized = []

workpath = os.path.dirname(os.path.abspath(__file__)) #Returns the Path your .py file is in

with open(os.path.join(workpath, 'data-BA77E.csv'), 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        try:
            data[row[2]] = float(row[4])
        except:
            uncategorized.append(row[2])
            continue
       
region = list(data.keys())

for key, val in data.items():
    if val > 4:
        rHigh.append(key)
    elif 1 <= val and val <= 4:
        rMedium.append(key)
    elif val < 1:
        rLow.append(key)
        

def getRHigh():
    return rHigh

def getRMedium():
    return rMedium

def getRLow():
    return rLow

def getAllRegion():
    return region + uncategorized;

def getRegionTuple():
    reglst = []
    for reg in getAllRegion():
        tupl = (reg, reg)
        reglst.append(tupl)
    return reglst

