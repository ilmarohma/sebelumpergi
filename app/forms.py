from django import forms
from .models import *

class FormDemografi(forms.ModelForm):
	class Meta:
		model = ModelDemografi
		fields = '__all__'
		widgets = {
		'nama':forms.TextInput(attrs={'class':'form-control',
											'placeholder':'Masukkan nama lengkap Anda',
											'id':'nama_input'}),
		'daerah_asal':forms.Select(attrs={'class':'form-control', 
											'id':'daerah_asal_input'}),
		'daerah_tujuan':forms.Select(attrs={'class':'form-control', 
											'id':'daerah_asal_input'}),
		'apakah_pernah_kontak_dengan_pasien_positif_Covid19_atau_pernah_berkunjung_ke_daerah_endemi_dalam_14_hari_terakhir':
		forms.Select(attrs={'class':'form-control'}),
		'sedang_atau_pernah_mengalami_deman_pilek_batuk_sesak_napas': forms.Select(attrs={'class':'form-control'}),
		'selama_14_hari_karantina_diri_Anda_mengalami': forms.Select(attrs={'class':'form-control'})
		}

class FormQuestionHigh(forms.ModelForm):
	class Meta:
		model = ModelQuestionHigh
		fields = '__all__'
		widgets = {
		'apakah_dalam_kurun_waktu_14_hari_anda_pernah_bepergian_ke_luar_negeri_atau_daerah_yang_berisiko_covid_tinggi':
		forms.Select(attrs={'class':'form-control'}),
		'apakah_dalam_kurun_waktu_14_hari_anda_pernah_berkontak_atau_serumah_dengan_orang_yang_positif_covid': forms.Select(attrs={'class':'form-control'}),
		'apakah_dalam_kurun_waktu_14_hari_anda_mengalami_kesulitan_bernafas_nyeri_dada_merasa_sulit_untuk_bangun_dan_penurunan_kesadaran': forms.Select(attrs={'class':'form-control'}),
		'apakah_dalam_kurun_waktu_14_hari_anda_menjadi_pekerja_kesehatan': forms.Select(attrs={'class':'form-control'}),
		'apakah_ada_memiliki_penyakit_kronis_seperti_kardiovaskular_diabetes_penyakit_ginjal_dan_cancer': forms.Select(attrs={'class':'form-control'})
		}

class FormQuestionMedium(forms.ModelForm):
	class Meta:
		model = ModelQuestionMedium
		fields = '__all__'
		widgets = {
		'apakah_dalam_kurun_waktu_14_hari_anda_pernah_berkontak_dengan_orang_yang_PDP':
		forms.Select(attrs={'class':'form-control'}),
		'apakah_dalam_kurun_waktu_14_hari_anda_pernah_berada_di_kerumunan': forms.Select(attrs={'class':'form-control'}),
		'apakah_dalam_kurun_waktu_14_hari_anda_pernah_keluar_rumah_tanpa_masker': forms.Select(attrs={'class':'form-control'}),
		}

class FormQuestionLow(forms.ModelForm):
	class Meta:
		model = ModelQuestionLow
		fields = '__all__'
		widgets = {
		'apakah_dalam_kurun_waktu_14_hari_anda_pernah_keluar_rumah_dan_menaati_protokol_kesehatan':
		forms.Select(attrs={'class':'form-control'}),
		'apakah_dalam_kurun_waktu_14_hari_anda_tidak_pernah_keluar_rumah': forms.Select(attrs={'class':'form-control'}),
		'apakah_dalam_kurun_waktu_14_hari_anda_mengalami_kesulitan_bernafas_,nyeri_dada_,merasa_sulit_untuk_bangun_dan_penurunan_kesadaran': forms.Select(attrs={'class':'form-control'}),
		'apakah_dalam_kurun_waktu_14_hari_anda_rutin_minum_vitamin_dan_makan_makanan_bergizi': forms.Select(attrs={'class':'form-control'}),
		}

# class MultiForm(MultiModelForm):
# 	form_classes = {
# 		'demografi': FormDemografi,
# 		'questionHigh' : FormQuestionHigh,
# 		'questionMedium' : FormQuestionMedium,
# 		'questionLow' : FormQuestionLow,
# 	}