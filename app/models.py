from django.db import models
from .data import *
# Create your models here.

class ModelDemografi(models.Model):
	daerah = getRegionTuple()
	nama = models.TextField(max_length=100)
	daerah_asal = models.CharField(max_length=100, choices=daerah)
	daerah_tujuan = models.CharField(max_length=100, choices=daerah)
	def __str__(self):
		return self.nama

class ModelQuestionHigh(models.Model):
	ANSWER = (
        ('3','YA'),
        ('0','TIDAK'),
    )
	apakah_dalam_kurun_waktu_14_hari_anda_pernah_bepergian_ke_luar_negeri_atau_daerah_yang_berisiko_covid_tinggi = models.CharField(default='Y',max_length=1, choices=ANSWER)
	apakah_dalam_kurun_waktu_14_hari_anda_pernah_berkontak_atau_serumah_dengan_orang_yang_positif_covid = models.CharField(default='Y',max_length=1, choices=ANSWER)
	apakah_dalam_kurun_waktu_14_hari_anda_mengalami_kesulitan_bernafas_nyeri_dada_merasa_sulit_untuk_bangun_dan_penurunan_kesadaran = models.CharField(default='Y',max_length=1, choices=ANSWER)
	apakah_dalam_kurun_waktu_14_hari_anda_menjadi_pekerja_kesehatan = models.CharField(default='Y',max_length=1, choices=ANSWER)
	apakah_ada_memiliki_penyakit_kronis_seperti_kardiovaskular_diabetes_penyakit_ginjal_dan_cancer = models.CharField(default='Y',max_length=1, choices=ANSWER)

class ModelQuestionMedium(models.Model):
	ANSWER = (
        ('2','YA'),
        ('0','TIDAK'),
    )
	apakah_dalam_kurun_waktu_14_hari_anda_pernah_berkontak_dengan_orang_yang_PDP = models.CharField(default='Y',max_length=1, choices=ANSWER)
	apakah_dalam_kurun_waktu_14_hari_anda_pernah_berada_di_kerumunan = models.CharField(default='Y',max_length=1, choices=ANSWER)
	apakah_dalam_kurun_waktu_14_hari_anda_pernah_keluar_rumah_tanpa_masker = models.CharField(default='Y',max_length=1, choices=ANSWER)

class ModelQuestionLow(models.Model):
	ANSWER = (
        ('1','YA'),
        ('0','TIDAK'),
    )
	apakah_dalam_kurun_waktu_14_hari_anda_pernah_keluar_rumah_dan_menaati_protokol_kesehatan = models.CharField(default='Y',max_length=1, choices=ANSWER)
	apakah_dalam_kurun_waktu_14_hari_anda_tidak_pernah_keluar_rumah = models.CharField(default='Y',max_length=1, choices=ANSWER)
	apakah_dalam_kurun_waktu_14_hari_anda_rutin_minum_vitamin_dan_makan_makanan_bergizi = models.CharField(default='Y',max_length=1, choices=ANSWER)

